import { Component, OnInit }    from '@angular/core';
import { Router }               from '@angular/router';
import { UserService }          from '../service/user.service';
import { BudgetService }        from '../service/budget.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  projectName: string;
  projectDescription: string;
  error: any;
  success: any;

  constructor( private userService: UserService, private budgetService: BudgetService, private router: Router ) { }

  ngOnInit() {
  }

  clear(){
    this.error = undefined;
    this.success = undefined;
  }

  createBudget(){
    if ( this.projectName && this.projectDescription ){

      this.budgetService.newBudget(this.projectName, this.projectDescription)
        .subscribe (
          res => { this.success = 'Budget was created!'},
          (err) => {this.error = err},
          () => {

          });
    } else {
      this.error = 'Missing parameters!';
    }

  }

  logout(){
    this.userService.logout();
    this.router.navigate(['/login']);
  }

}
