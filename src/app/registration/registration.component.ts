import { Component, OnInit } from '@angular/core';
import { Router }               from '@angular/router';

import { UserService }       from '../service/user.service';
import { CookieService }     from 'ngx-cookie-service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  email: string;
  name: string;
  password: string;
  password_confirmation: string;
  error: string;

  constructor(private userService: UserService, private router: Router ) { }

  ngOnInit() {
    this.clear();
  }

  clear(){
    this.error = undefined;
  }

  registration(){
    if ( this.name && this.email && this.password && this.password_confirmation ) {
      this.userService.createUser(this.name, this.email, this.password, this.password_confirmation)
        .subscribe (
          data => {},
          (err) => {this.error = err},
          () => {
            this.userService.getKey( this.email, this.password )
              .subscribe (
                data => this.userService.login( data.auth_token ),
                (err) => {this.error = err},
                () => {
                  if ( this.userService.isLogged() ){
                    this.router.navigate(['/dashboard']);
                  }
                });
          }
        );
    } else {
      this.error = 'Empty input error!';
    }
  }
}
