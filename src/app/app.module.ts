// ANGULAR IMPORTS
import { BrowserModule }            from '@angular/platform-browser';
import { BrowserAnimationsModule }  from '@angular/platform-browser/animations';
import { NgModule, ApplicationRef } from '@angular/core';
import { FormsModule }              from '@angular/forms';
import { CommonModule }             from '@angular/common';
import { AgmCoreModule }            from '@agm/core';
import { HttpClientModule }         from '@angular/common/http';
import { AppRoutingModule }         from './/app-routing.module';

// MODULES
import { MatNativeDateModule,
         MatFormFieldModule ,
         MatInputModule,
         MatIconModule,
         MatDatepickerModule }      from '@angular/material';

// COMPONENTS
import { AppComponent }             from './app.component';
import { LoginComponent }           from './login/login.component';
import { RegistrationComponent }    from './registration/registration.component';
import { DashboardComponent }       from './dashboard/dashboard.component';
import { AboutComponent }           from './about/about.component';
import { ChartTimelineComponent }   from './chart-timeline/chart-timeline.component';
import { NavigationComponent }      from './navigation/navigation.component';
import { DashboardMenuComponent }   from './dashboard-menu/dashboard-menu.component';
import { CreatePaymentComponent }   from './create-payment/create-payment.component';
import { BudgetReviewComponent }    from './budget-review/budget-review.component';
import { AnalysisComponent }        from './analysis/analysis.component';
import { PiechartComponent }        from './piechart/piechart.component';
import { PolarchartComponent }      from './polarchart/polarchart.component';
import { LinechartComponent }       from './linechart/linechart.component';


// SERVICES
import { BudgetService }            from './service/budget.service';
import { UserService }              from './service/user.service';

import { CookieService }            from 'ngx-cookie-service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    DashboardComponent,
    AboutComponent,
    ChartTimelineComponent,
    NavigationComponent,
    DashboardMenuComponent,
    CreatePaymentComponent,
    BudgetReviewComponent,
    AnalysisComponent,
    PiechartComponent,
    PolarchartComponent,
    LinechartComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    CommonModule,
    HttpClientModule, // import HttpClientModule after BrowserModule.
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCtgjyFY52batG4tVVbr-KqXr_JTG3mtSU'
    }),
    MatNativeDateModule,
    MatFormFieldModule ,
    MatInputModule,
    MatIconModule,
    MatDatepickerModule
  ],
  providers: [
      BudgetService,
      UserService,
      CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
