class Payment {
  id: number;
  amount: number;
  budget_item_id: number;
  created_at: string;
  updated_at: string;
  user_id: number;

  constructor(id: number, amount: number, budget_item_id: number, created_at: string, updated_at: string, user_id: number){
    this.id = id;
    this.amount = amount;
    this.budget_item_id = budget_item_id;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.user_id = user_id;
  }
}
