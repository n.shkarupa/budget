class Budget {

  id: number;
  name: string;
  description: string;
  created_at: string;

  constructor(id: number, name: string, description: string, created_at: string){
    this.id = id;
    this.name = name;
    this.description = description;
    this.created_at = created_at;
  }
}
