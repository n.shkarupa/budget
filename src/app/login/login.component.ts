import { Component, OnInit }    from '@angular/core';
import { Router }               from '@angular/router';

import { UserService }          from '../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  error: string;
  email: string;
  password: string;

  constructor ( private userService: UserService, private router: Router ) {}

  ngOnInit(): void {
    if ( this.userService.isLogged() ){
      this.router.navigate(['/dashboard']);
    }
    this.clear();
  }

  clear(){
    this.error = undefined;
  }

  authorization() {
    if (this.email && this.password){
      this.userService.getKey(this.email, this.password)
      .subscribe (
        data => {this.userService.login(data.auth_token)},
        (err) => {this.error = err},
        () => {
          if ( this.userService.isLogged() ){
            this.router.navigate(['/dashboard']);
          }
        });
    } else { this.error = 'No login or password provided!'; }
  }

}
