import { NgModule }               from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';

import { DashboardComponent }     from './dashboard/dashboard.component';
import { LoginComponent }         from './login/login.component';
import { RegistrationComponent }  from './registration/registration.component';
import { AboutComponent }         from './about/about.component';
import { ChartTimelineComponent } from './chart-timeline/chart-timeline.component';
import { DashboardMenuComponent } from './dashboard-menu/dashboard-menu.component';
import { CreatePaymentComponent } from './create-payment/create-payment.component';
import { BudgetReviewComponent }  from './budget-review/budget-review.component';
import { AnalysisComponent }      from './analysis/analysis.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent,
    children: [
      { path: '', component: DashboardMenuComponent },
      { path: 'about', component: AboutComponent },
      { path: 'timeline', component: ChartTimelineComponent },
      { path: 'create-payment', component: CreatePaymentComponent },
      { path: 'budget-review', component: BudgetReviewComponent },
      { path: 'analysis', component: AnalysisComponent }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent }
  // { path: 'detail/:id', component: UserDetailComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
