import { Component, OnInit }  from '@angular/core';
import { Chart }              from 'chart.js';

import { BudgetService }      from '../service/budget.service';

import '../model/budget-item';
import '../model/budget';
import '../model/payment';

@Component({
  selector: 'app-chart-timeline',
  templateUrl: './chart-timeline.component.html',
  styleUrls: ['./chart-timeline.component.css']
})
export class ChartTimelineComponent implements OnInit {

  budgets: any[];
  selectedBudget: any;
  budgetItems: BudgetItem[];
  datasets: any[];
  payments: Payment[];
  chart = [];
  dates: any[] = [];
  colors = ["#c0ebc9", "#723afc", "#2387b0", "#7abb4a", "#f5c859", "#68ddd2", "#4469b4", "#fa6c0b"];

  month: number;
  days = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];

  constructor(private budgetService: BudgetService) { }

  ngOnInit() {
    this.budgetService.getBudgets()
      .subscribe(res => {
        this.budgets = res as Budget[];
        this.buildChart();
      });
  }

  setUpDates(){
    for (let i = 0; i < 32; i++){
      let date = new Date(2018, this.month, i);
      this.dates.push(date);
    }
  }


  loadBudgetItems(){
    this.budgetService.getBudgetItems( this.selectedBudget )
      .subscribe( res => {
        this.budgetItems = res as BudgetItem[];
      },
      (err) => console.error(err),
      () => { this.loadPayments(); }
    );
  }

  loadDatasets(){
    let arr : dataset[] = [];
    for (var i = 0; i < this.budgetItems.length; i++) {
      let data = new dataset(this.budgetItems[i].name, this.colors[i], this.budgetItems[i].payments);
      arr.push(data);
    }
    this.datasets = arr;
    this.buildChart();
  }

  loadPayments(){
    this.budgetService.getPayments()
      .subscribe(res => {
        this.payments = res as Payment[];
      },
      (err) => console.error(err),
      () => {
        for (let i = 0; i < this.budgetItems.length; i++){
          let arr = [];
          for (let a = 0; a < this.payments.length; a++) {
            if (this.budgetItems[i].id == this.payments[a].budget_item_id){
              arr.push(this.payments[a].amount);
            }
          }
          this.budgetItems[i].payments = arr;
          console.log(this.budgetItems[i]);
          this.loadDatasets();
        }
      });
  }


  buildChart() {
    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: this.days,
        datasets: this.datasets
      },
      options: {
        responsive: true,
        title: {
          display: false,
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Timestamp'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Spending $'
            }
          }]
        }
      }
    })
  }

}

class dataset {
  label: any;
  backgroundColor: any;
  borderColor: any;
  data: any;
  fill: any;
  constructor (label: any, backgroundColor: any, data: any){
    this.label = label;
    this.backgroundColor = backgroundColor;
    this.borderColor = backgroundColor;
    this.data = data;
    this.fill = false;
  }
}
