import { Injectable }               from '@angular/core';
import { HttpClient, HttpHeaders }  from '@angular/common/http';
import { Observable }               from 'rxjs/Observable';
import { ErrorObservable }          from 'rxjs/observable/ErrorObservable';
import { catchError, retry }        from 'rxjs/operators';
import { HttpErrorResponse }        from '@angular/common/http';
import { CookieService }            from 'ngx-cookie-service';
import * as jwt_decode              from "jwt-decode";

@Injectable()
export class UserService {

  _baseUrl = 'http://localhost:3000/api/v1/';

  constructor (private http: HttpClient, private cookieService: CookieService) {}


  getKey(email: string, password: string) {
      return this.http.post( this._baseUrl.concat('users/login'), {email: email, password: password})
      .pipe( catchError(this.handleError) );
  }

  createUser(name: string, email: string, password: string, password_confirmation: string) {
    return this.http.post(this._baseUrl.concat('users'), {
      user: {
        name: name,
        email: email,
        password: password,
        nae: password_confirmation
      }})
      .pipe(catchError(this.handleError));
  }

  isLogged(): boolean{
    return (this.cookieService.check('token'));
  }

  login(token: any) {
    if (!this.isLogged() && token){
      let id = this.getId(token).user_id;
      this.cookieService.set('id', id);
      this.cookieService.set('token', token);
    }
  }

  logout(){
    if (this.isLogged()) {
      this.cookieService.set('id', '', -1);
      this.cookieService.set('token', '', -1);
    }
  }

  getId(token: string): any{
    try {
        return jwt_decode(token);
    }
    catch(Error) {
        console.error("Failed to decode token!");
        return null;
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.warn(`Response status ${error.status}.`);
    }
    return new ErrorObservable(error.error);
  };

}
