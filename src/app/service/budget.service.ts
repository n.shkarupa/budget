import { Injectable }               from '@angular/core';
import { HttpClient, HttpHeaders }  from '@angular/common/http';
import { CookieService }            from 'ngx-cookie-service';
import { UserService }              from '../service/user.service';


import { Observable } from 'rxjs/Rx';
import { Http, Headers, RequestOptions, Request, RequestMethod, Response } from '@angular/http';

import '../model/budget-item';


@Injectable()
export class BudgetService {

  constructor( private _http: HttpClient,  private userService: UserService, private cookieService: CookieService ) {}

  _baseUrl : string = 'http://localhost:3000/api/v1/';

  httpOptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + this.cookieService.get('token')
    })
  };

  newBudget(name: string, description: string){
    let url = `${this._baseUrl}/budgets`;
    return this._http.post( url, {
      budget: {
        name: name,
        description: description,
      }}, this.httpOptions );
  }

  getBudgets(){
    let url = `${this._baseUrl}/budgets`;
    return this._http.get( url, this.httpOptions );
  }

  newBudgetItem(budgetId: string, name: string){
    let url = `${this._baseUrl}/budgets/${budgetId}/budget_items`;
    return this._http.post( url, {
      budget_item: {
        name: name,
      }}, this.httpOptions);
  }

  getBudgetItems(budgetId: string){
    return this._http.get(this._baseUrl.concat('budgets/').concat(budgetId).concat('/budget_items'), this.httpOptions);
  }


  newPayment(amount: number, description: string, budget_item_id: number, date: string){
    let url = `${this._baseUrl}/payments`;
    return this._http.post(url, {
      	payment:
      	{
      		amount: amount,
      		description: description,
      		budget_item_id: budget_item_id,
      		date: date
      	}
    }, this.httpOptions );
  }

  getPayments(){
    let url = `${this._baseUrl}payments/`;
    return this._http.get(url, this.httpOptions);
  }

  users(){
    return null;
  }


}
