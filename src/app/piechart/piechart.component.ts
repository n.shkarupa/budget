import { Component, OnInit } from '@angular/core';
import { Chart }             from 'chart.js';

import { BudgetService }     from '../service/budget.service';

@Component({
  selector: 'app-piechart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.css']
})
export class PiechartComponent implements OnInit {

    chart = [];

    constructor(private budgetService: BudgetService) { }

    ngOnInit() {
      this.budgetService.getBudgets()
        .subscribe(res => {

        let data = [1, 2, 3, 4, 5]
        let colors = ["rgba(251, 173, 180, 0.7)", "rgba(255, 223, 186, 0.7)", "rgba(237, 237, 175, 0.7)", "rgba(161, 218, 174, 0.7)", "rgba(168, 202, 228, 0.7)"]
        let borderColor = ["rgba(251, 173, 180, 1)", "rgba(255, 223, 186, 1)", "rgba(237, 237, 175, 1)", "rgba(161, 218, 174, 1)", "rgba(168, 202, 228, 1)"]

        this.chart = new Chart('piechart', {
            type: 'pie',
    		data: {
    			datasets: [{
    				data: data,
    				backgroundColor: colors,
                    borderColor: borderColor,
    				label: 'Dataset 1'
    			}],
    			labels: [
    				'Red',
    				'Orange',
    				'Yellow',
    				'Green',
    				'Blue'
    			]
    		},
    		options: {
    			responsive: true
    		}
        })  // piechart end

      }); // Subscribe end
    }

}
