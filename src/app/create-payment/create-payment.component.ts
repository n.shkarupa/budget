import { Component, OnInit } from '@angular/core';
import { BudgetService }     from '../service/budget.service';

@Component({
  selector: 'app-create-payment',
  templateUrl: './create-payment.component.html',
  styleUrls: ['./create-payment.component.css']
})
export class CreatePaymentComponent implements OnInit {

  constructor( private budgetService: BudgetService ) { }

  budgets : Budget[];
  budgetItems : BudgetItem[];
  selectedBudget: any;
  budgetItemName: any;
  selectedBudgetItem: any;
  created : any;
  selectedDate: string;
  selectedAmount: number;
  purposeText: string;

  ngOnInit() {
    this.loadBudgets();
    this.created = undefined;
  }

  loadBudgets() {
    this.budgetService.getBudgets()
      .subscribe( res => {
        this.budgets = res as Budget[];
      } );
  }

  loadBudgetItems(){
    this.budgetService.getBudgetItems(this.selectedBudget)
      .subscribe(
        res => {
          this.budgetItems = res as BudgetItem[];
        }
      )
  }

  createBudgetItem(){
    this.budgetService.newBudgetItem(this.selectedBudget, this.budgetItemName)
      .subscribe (
        error => {}
      );
    this.loadBudgetItems()
  }

  createPayment(){
    this.budgetService.newPayment( this.selectedAmount, this.purposeText, this.selectedBudgetItem, this.selectedDate )
    .subscribe (
      error => {}
    );
    this.created = true;
    this.clear();
  }

  clear(){
    this.budgetItems = undefined;
    this.selectedBudget = undefined;
    this.budgetItemName = undefined;
    this.selectedBudgetItem = undefined;
    this.selectedDate = undefined;
    this.selectedAmount = undefined;
    this.purposeText = undefined;
  }

}

interface Budget {
  id: number;
  name: string;
  description: string;
  created_at: string;
}

interface BudgetItem {
  id: number;
  name: string;
  budget_id: number;
  created_at: string;
  updated_at: string;
  user_id: number;
}
