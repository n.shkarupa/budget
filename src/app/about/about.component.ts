import { Component } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})

export class AboutComponent  {
  // google maps zoom level
  zoom: number = 14;

  // initial center position for the map
  lat: number = 50.102673;
  lng: number = 14.393013;

  markers: marker[] = [
	  {
		  lat: 50.102673,
		  lng: 14.393013,
		  draggable: false
	  }
  ]
}

// just an interface for type safety.
interface marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}
