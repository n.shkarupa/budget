import { Component, OnInit }  from '@angular/core';
import { BudgetService }      from '../service/budget.service';
import '../model/budget';
import '../model/budget-item';
import '../model/payment';

@Component({
  selector: 'app-budget-review',
  templateUrl: './budget-review.component.html',
  styleUrls: ['./budget-review.component.css']
})
export class BudgetReviewComponent implements OnInit {

  warning: any;
  error: string;
  selectedBudget: any;
  selectedBudgetItem: any;
  budgets: Budget[];
  budgetItems: BudgetItem[];
  payments: Payment[];

  constructor(private budgetService: BudgetService) { }

  ngOnInit() {
    this.budgetService.getBudgets()
      .subscribe(res => {
        this.budgets = res as Budget[];
      },
    (err) => {this.error = err},
    () => {
      if (this.budgets.length == 0){
        this.warning = true;
      }
    });
  }

  loadBudgetItems(){
    this.budgetService.getBudgetItems(this.selectedBudget)
      .subscribe( res => {
        this.budgetItems = res as BudgetItem[];
      });
  }

  loadStatistic(){
    this.budgetService.getPayments()
      .subscribe(res => {
        this.payments = res as Payment[];
      },
      (err) => console.error(err),
      () => {
        let arr : Payment[] = [];
        let b = this.selectedBudgetItem;
        this.payments.forEach(function(value){
          if (value.budget_item_id == b){
            arr.push(value)
          }
        });
        this.payments = arr;
      }
    );
  }

}
